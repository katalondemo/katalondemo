import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.mouseOver(findTestObject('CreateCVR_OR/Menu/Order_menu'))

//Calling a selectModule Keyword
CustomKeywords.'cvr.SelectMenu.selectModule'(1)

WebUI.click(findTestObject('CreateCVR_OR/Choose_Customers/Add new'))

WebUI.click(findTestObject('CreateCVR_OR/Choose_Customers/Select Customer'))

WebUI.click(findTestObject('CreateCVR_OR/Choose_Customers/ChooseCustomer'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(2)

WebUI.click(findTestObject('CreateCVR_OR/Choose_Customers/button_Add New'))

WebUI.click(findTestObject('CreateCVR_OR/AddSales/ProductDDL'))

WebUI.click(findTestObject('CreateCVR_OR/AddSales/SelectProduct'))

WebUI.click(findTestObject('CreateCVR_OR/AddSales/OpenProduct'))

WebUI.setText(findTestObject('CreateCVR_OR/AddSales/InputPotential'), potential)

WebUI.setText(findTestObject('CreateCVR_OR/AddSales/InputRequirement'), requirement)

WebUI.setText(findTestObject('CreateCVR_OR/AddSales/InputComments'), comments)

WebUI.delay(2)

WebUI.click(findTestObject('CreateCVR_OR/AddSales/BtnAddProduct'))

WebUI.click(findTestObject('CreateCVR_OR/AddSales/BtnSubmit'))

WebUI.delay(2)

