import static com.kms.katalon.core.testdata.TestDataFactory.findTestData

import java.sql.ResultSet

import org.testng.Assert

//String query= findTestData('CVR/TD_CVR_Collection').getValue(5, 1)

ResultSet rs = CustomKeywords.'cvr.ExecuteQuery.connectAndExecuteQuery'(sqlQuery)

double DBamount;
double DBchequeNo;
String DBbankName;
double DBinvoiceNo;
while (rs.next())
{
	DBamount = rs.getDouble(6);
	DBchequeNo = rs.getDouble(15);
	DBbankName = rs.getString(16);
	DBinvoiceNo =rs.getDouble(4);
}

Assert.assertEquals(DBamount, UIAmount.toDouble())
Assert.assertEquals(DBchequeNo, UIChequeNo.toDouble())
Assert.assertEquals(DBbankName, UIBankName)
Assert.assertEquals(DBinvoiceNo, UIInvoiceNo.toDouble())