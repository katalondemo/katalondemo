import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.testobject.RequestObject as RequestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

username = (parentUid.toString() + uid.toString())

uname = name.toString()

password = uname.substring(0, 5)

WebUI.openBrowser('')

WebUI.navigateToUrl(url)

WebUI.waitForElementPresent(findTestObject('Login_OR/input_username'), 5)

WebUI.setText(findTestObject('Login_OR/input_username'), username)

WebUI.setText(findTestObject('Login_OR/input_password'), password)

WebUI.click(findTestObject('Login_OR/Btn_login'))

WebUI.verifyElementText(findTestObject('Login_OR/VerifyText_Executive_Dashboard'), validationText)

