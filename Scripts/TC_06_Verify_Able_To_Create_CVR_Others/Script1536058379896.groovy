import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import cvr.Customer as Customer

//Calling a selectModule Keyword
CustomKeywords.'cvr.SelectMenu.selectModule'(4)

//Creating a Test Object
to_AddNew = findTestObject('CreateCVR_OR/Choose_Customers/button_Add_New3')

//Creating an Object for Customer class
Customer c = new Customer()

//Calling a parameterized method
c.selectCustomer(to_AddNew)

WebUI.setText(findTestObject('CreateCVR_OR/AddCVROthers/InputComments'), comments)

WebUI.click(findTestObject('CreateCVR_OR/AddCVROthers/ProductGroupDDL'))

WebUI.click(findTestObject('CreateCVR_OR/AddCVROthers/SelectProduct'))

WebUI.setText(findTestObject('CreateCVR_OR/AddCVROthers/InputQuantity'), quantity)

WebUI.delay(2)

WebUI.click(findTestObject('CreateCVR_OR/AddCVROthers/BtnAddProduct'))

WebUI.click(findTestObject('CreateCVR_OR/AddCVROthers/BtnSaveChanges'))

WebUI.delay(3)

WebUI.closeBrowser()

