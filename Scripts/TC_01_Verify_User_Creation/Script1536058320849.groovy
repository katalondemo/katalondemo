import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.testdata.TestData as TestData
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import com.kms.katalon.core.testobject.ConditionType as ConditionType
import com.kms.katalon.core.testobject.RequestObject as RequestObject
import com.kms.katalon.core.testobject.TestObjectProperty as TestObjectProperty
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as gv

RequestObject request = findTestObject('Login_OR/createuser', [('name') : name,('uid') : uid, ('puid') : parentUid, ('url') :url])

def output = WS.sendRequest(request)

Iterator listIterator = request.getHttpHeaderProperties().listIterator()

while (listIterator.hasNext()) 
{
    TestObjectProperty p = ((listIterator.next()) as TestObjectProperty)
    requestHeader = ((((requestHeader + p.getName()) + '=') + p.getValue()) + '\n')
}

WebUI.comment((((('\nrequest:\n' + request.restUrl) + '\n\nrequest header:\n') + requestHeader) + '\nrequest data:\n') + 
    request.httpBody)

WS.verifyResponseStatusCode(output, 200)

output.getBodyContent()

WS.verifyElementPropertyValue(output, 'success', 'true')

WS.verifyElementPropertyValue(output, 'result', 'Successfully updated user details.')