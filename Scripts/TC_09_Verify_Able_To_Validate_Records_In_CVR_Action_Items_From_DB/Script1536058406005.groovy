import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import java.sql.ResultSet
import com.kms.katalon.core.util.KeywordUtil
import internal.GlobalVariable as gv
import org.testng.Assert


ResultSet rs = CustomKeywords.'cvr.ExecuteQuery.connectAndExecuteQuery'(sqlQuery)


String DBactionItems;

while (rs.next())
{
	DBactionItems = rs.getString(7);
}

if(!DBactionItems.equals(UIActionItems))
{
	gv.reason = "ActionItems from DB and UI are not equal"
	KeywordUtil.markFailed(gv.reason)
}