import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import java.sql.ResultSet as ResultSet
import org.testng.Assert as Assert
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import internal.GlobalVariable as GlobalVariable
import internal.GlobalVariable as gv
import com.kms.katalon.core.util.KeywordUtil

ResultSet rs = CustomKeywords.'cvr.ExecuteQuery.connectAndExecuteQuery'(sqlQuery)

String DBcomments

double DBquantity

while (rs.next()) {
    DBcomments = rs.getString(7)

    DBquantity = rs.getDouble(2)
}

//Assert.assertEquals(DBcomments, UIComments)

//Assert.assertEquals(DBquantity, UIQuantity.toDouble())


if(!DBcomments.equals(UIComments))
{
	gv.reason = "Comments from DB and UI are not equal"
	KeywordUtil.markFailed(gv.reason)
}

if(!DBquantity.equals(UIQuantity.toDouble()))
{
	gv.reason = "Quantity from DB and UI are not equal"
	KeywordUtil.markFailed(gv.reason)
}
