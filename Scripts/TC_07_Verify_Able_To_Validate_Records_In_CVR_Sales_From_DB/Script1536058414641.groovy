import static com.kms.katalon.core.testdata.TestDataFactory.findTestData

import java.sql.DriverManager
import java.sql.ResultSet

import org.testng.Assert

import com.mysql.jdbc.Connection
import com.mysql.jdbc.Statement


//sql= findTestData('CVR/TD_CVR_Sales').getValue(4, 1)

ResultSet rs = CustomKeywords.'cvr.ExecuteQuery.connectAndExecuteQuery'(sqlQuery)

double DBpotential;
double DBreq;
String DBcomments;
while (rs.next())
{
	DBpotential = rs.getDouble(6);
	DBreq = rs.getDouble(14);
	DBcomments = rs.getString(12);
}

Assert.assertEquals(DBpotential, UIPotential.toDouble())
Assert.assertEquals(DBreq, UIRequirement.toDouble())
Assert.assertEquals(DBcomments, UIComments)
