

import java.sql.DriverManager
import java.sql.ResultSet
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import com.kms.katalon.core.annotation.Keyword
import com.mysql.jdbc.Connection
import com.mysql.jdbc.Statement

public class ExecuteQuery {
	@Keyword
	public ResultSet connectAndExecuteQuery(String query) {
		Connection con1 = DriverManager.getConnection(findTestData('Database/TD_DBDetails').getValue(1, 1), findTestData('Database/TD_DBDetails').getValue(2, 1), findTestData('Database/TD_DBDetails').getValue(3, 1))

		Statement stmt = con1.createStatement();

		ResultSet rs= stmt.executeQuery(query);
	}
}
