

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords

import internal.GlobalVariable

import MobileBuiltInKeywords as Mobile
import WSBuiltInKeywords as WS
import WebUiBuiltInKeywords as WebUI

public class Customer {
	public void selectCustomer() {
		WebUI.click(findTestObject('CreateCVR_OR/Choose_Customers/Add_new2'))

		WebUI.click(findTestObject('CreateCVR_OR/Choose_Customers/Select_Customer2'))

		WebUI.click(findTestObject('CreateCVR_OR/Choose_Customers/ChooseCustomer2'))

		WebUI.delay(2)

		WebUI.click(findTestObject('CreateCVR_OR/Choose_Customers/button_Add_New2'))
	}
	public void selectCustomer(TestObject to) {
		WebUI.click(findTestObject('CreateCVR_OR/Choose_Customers/Add_new2'))

		WebUI.click(findTestObject('CreateCVR_OR/Choose_Customers/Select_Customer2'))

		WebUI.click(findTestObject('CreateCVR_OR/Choose_Customers/ChooseCustomer2'))

		WebUI.delay(2)

		WebUI.click(to)
	}
}
